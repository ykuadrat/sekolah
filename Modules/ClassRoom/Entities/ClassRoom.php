<?php

namespace Modules\ClassRoom\Entities;

use App\Components\BaseModel;
use App\Models\School;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassRoom extends BaseModel
{

	use SoftDeletes;

	protected $guarded = [];
	protected $table = 'classes';

	public static function rule($class)
	{
		return [
			'name' => 'required|string',
			'grade' => 'required|numeric',
			'school_id' => 'required|integer'
		];
	}

	public function school()
	{
		return $this->belongsTo(School::class);
	}

}
