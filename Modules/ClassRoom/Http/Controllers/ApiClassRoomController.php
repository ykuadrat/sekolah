<?php

namespace Modules\ClassRoom\Http\Controllers;

use App\Components\Traits\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\ClassRoom\Entities\ClassRoom;
use Spatie\QueryBuilder\QueryBuilder;

class ApiClassRoomController extends Controller
{

    use ApiController;

    public function datatable()
    {
        return \DataTables::of(ClassRoom::with('school'))
                          ->addColumn('action', function($row){
                            return \DatatableBuilderHelper::button([
                                'edit' => [
                                    'url' => route('class.edit', ['id' => $row->id]), 
                                    'data' => str_replace('"', "'", $row->toJson()),
                                    'title' => 'Edit Class'
                                ],
                                'delete' => route('class.destroy', ['id' => $row->id]),
                            ]);
                          })
                          ->rawColumns(['action'])
                          ->make(true);
    }

    public function index()
    {
        $paginate = request()->has('paginate') && request()->paginate > 0 ? request()->paginate : 100; 
        $classes = ClassRoom::simplePaginate($paginate);

        return $this->sendRes($classes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->merge(['school_id' => 1]);
        $class = new ClassRoom;
        $class->fillAndValidate()->save();

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $class]);
        }

        return $class;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClassRoom  $class
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $class = ClassRoom::findOrFail($id);

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $class]);
        }

        return $class;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClassRoom  $class
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        request()->merge(['school_id' => 1]);
        $class = ClassRoom::findOrFail($id);
        $class->fillAndValidate()->save();

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $class]);
        }

        return $class;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClassRoom  $class
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $class = ClassRoom::findOrFail($id);
        $class->delete();

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $class]);
        }

        return $class;
    }
}
