<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->prefix('class')->group(function() {
	Route::get('', 'ApiClassRoomController@index')->name('class.data');
	Route::get('{id}', 'ApiClassRoomController@show')->name('class.detail');
	Route::put('{id}', 'ApiClassRoomController@update')->name('class.edit');
	Route::post('create', 'ApiClassRoomController@store')->name('class.store');
	route::post('datatable', 'ApiClassRoomController@datatable')->name('class.datatable');
	Route::delete('/{id}', 'ApiClassRoomController@destroy')->name('class.destroy');
});
