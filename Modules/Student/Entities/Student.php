<?php

namespace Modules\Student\Entities;

use App\Components\BaseModel;
use App\Models\School;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends BaseModel
{

	use SoftDeletes;

	protected $guarded = ['class_id', 'generation'];

	public static function rule($student)
	{
		return [
			'name' => 'required|string',
			'class_id' => 'required|integer',
			'school_id' => 'required|integer'
		];
	}

	public function school()
	{
		return $this->belongsTo(School::class);
	}

	public function getClassAttribute()
	{
		return @$this->history_class()->orderBy('generation', 'desc')->first()->class;
	}

	public function history_class()
	{
		return $this->hasMany(StudentClass::class);
	}

}
