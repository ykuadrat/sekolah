<?php

namespace Modules\Student\Entities;

use App\Components\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\ClassRoom\Entities\ClassRoom;

class StudentClass extends BaseModel
{

	use SoftDeletes;

	protected $guarded = [];
	protected $table = 'student_class';

	public static function rule($brand)
	{
		return [
			'student_id' => 'required|integer',
			'school_id' => 'required|integer',
			'generation' => 'required|string'
		];
	}

	public function class()
	{
		return $this->belongsTo(ClassRoom::class);
	}

	public function student()
	{
		return $this->belongsTo(student::class);
	}

}
