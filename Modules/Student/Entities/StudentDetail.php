<?php

namespace Modules\Student\Entities;

use App\Components\BaseModel;
use App\Models\School;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentDetail extends BaseModel
{

	use SoftDeletes;

	protected $table = 'student_detail_view';

	public function school()
	{
		return $this->belongsTo(School::class);
	}

	public function history_class()
	{
		return $this->hasMany(StudentClass::class);
	}

}
