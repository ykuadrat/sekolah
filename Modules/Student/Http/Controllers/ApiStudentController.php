<?php

namespace Modules\Student\Http\Controllers;

use App\Components\Traits\ApiController;
use App\Http\Controllers\Controller;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentClass;
use Modules\Student\Entities\StudentDetail;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ApiStudentController extends Controller
{

    use ApiController;

    public function datatable()
    {
        $students = QueryBuilder::for(StudentDetail::class);
        // $students = QueryBuilder::for(Student::class)->allowedIncludes(['school']);
        // $students = QueryBuilder::for(Student::class)->allowedIncludes(['school'])
        //                         ->join('student_class AS sc', 'students.id', '=', 'sc.student_id')
        //                         ->join('classes AS c', 'sc.student_id', '=', 'c.id')
        //                         ->select('students.*', 'c.name AS class_name');

        return \DataTables::of(Student::with(['school']))
                          ->addColumn('class_name', function($row){
                            return $row->class->name;
                          })
                          ->addColumn('action', function($row){
                            $row->class = $row->class->toJson();

                            return \DatatableBuilderHelper::button([
                                'edit' => [
                                    'url' => route('student.update', ['id' => $row->id]), 
                                    'data' => str_replace('"', "'", $row->toJson()),
                                    'title' => 'Edit Student'
                                ],
                                'delete' => route('student.destroy', ['id' => $row->id]),
                            ]);
                          })
                          ->rawColumns(['action', 'class'])
                          ->make(true);
    }

    public function index()
    {
        $paginate = request()->has('paginate') && request()->paginate > 0 ? request()->paginate : 100; 
        $students = Student::simplePaginate($paginate);

        return $this->sendRes($students);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->merge(['school_id' => 1]);
        $student = new Student;
        $student->fillAndValidate()->save();

        StudentClass::create([
            'student_id' => $student->id,
            'class_id' => request()->class_id,
            'generation' => request()->generation
        ]);

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $student]);
        }

        return $student;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::findOrFail($id);

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $student]);
        }

        return $student;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        request()->merge(['school_id' => 1]);
        $student = Student::findOrFail($id);
        $student->fillAndValidate()->save();

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $student]);
        }

        return $student;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::findOrFail($id);
        $student->delete();

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $student]);
        }

        return $student;
    }
}
