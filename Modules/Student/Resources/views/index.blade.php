@extends('layouts.app', [
	'toolbarButton' => [
		['label' => 'Create new student', 'icon' => 'la la-plus', 'elOptions' => [
			'onclick' => "showModalForm({
				target: '#modalEdit', 
				title: 'Create New Student', 
				action: '". route('student.store') ."',
			})"
		]],
	]
])

@section('content')

{!! 
	DatatableBuilderHelper::render(route('student.datatable'), [
		'columns' => ['name', 'class_name', 'school.name', 'action'],
		'elOptions' => [
			'id' => 'test',
		],

	])
!!}

@include('components.modal_form_ajax', [
	'id' => 'modalEdit',
	'tableTarget' => 'test',
	'fields' => [
		Form::textInput('name', old('name')),
		[
			'field' => Form::select2Input('class_id', null, route('class.data') . '?paginate=25', [
				'pluginOptions' => [
					'dropdownParent' => '#modalEdit'
				]
			]),
			'selector' => '#select2-class_id',
			'type' => 'select2',

			// FOR SETTING DATA WHEN EDIT MODAL SHOWN
			'dataId' => 'class.id',
			'dataLabel' => 'class.name'
		],
		Form::textInput('generation', date('n') < 7 ? ((date('Y') - 1) . '/' . date('Y')) : (date('Y'). '/' . (date('Y') + 1))),
		Form::textareaInput('address', old('address'), ['elOptions' => ['rows' => 3]]),
	]
])

@endsection