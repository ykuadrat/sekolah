<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->prefix('student')->group(function () {
	Route::get('', 'ApiStudentController@index')->name('student.data');
	Route::get('{id}', 'ApiStudentController@show')->name('student.detail');
	Route::put('{id}', 'ApiStudentController@update')->name('student.update');
	Route::post('create', 'ApiStudentController@store')->name('student.store');
	route::post('datatable', 'ApiStudentController@datatable')->name('student.datatable');
	Route::delete('/{id}', 'ApiStudentController@destroy')->name('student.destroy');
});