<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('student')->group(function() {
	Route::get('/', 'StudentController@index')->name('student.list');
	Route::get('create', 'StudentController@create')->name('student.create');
	Route::get('{id}', 'StudentController@edit')->name('student.edit');
	Route::post('create', 'StudentController@store')->name('student.create');
	Route::delete('/{id}', 'StudentController@destroy')->name('student.delete');
});