<?php

namespace App\Components;

use App\Components\Filters\QueryFilters;
use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class BaseModel extends Model
{

	public function isNewRecord()
	{
		return empty($this->id);
	}
	
	public function fillAndValidate($customData = null, $rule = null)
	{
		$rule = $rule ?? static::rule($this);
		$data = $customData ?? request()->all();

		$validatedData = null;
		if (empty($customData)) {
			$validatedData = \Validator::make($data, $rule)->validate();
		} else {
			$validatedData = request()->validate($rule);
		}

		return parent::fill($validatedData);
	}

	/**
     * Filtering Berdasarakan Request User
     * @param $query
     * @param QueryFilters $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter($query, QueryFilters $filters)
    {
        return $filters->apply($query);
    }

}