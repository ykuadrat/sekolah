<?php

namespace App\Components\Filters;

use Illuminate\Http\Request;

class ClassFilters extends QueryFilters
{

    /**
     * Ordering data by name
     */
    public function q($value = 'all') {
        return (!$this->requestAllData($value)) ? $this->builder->where('classes.name', 'like', '%'.$value.'%') : null;
    }

}