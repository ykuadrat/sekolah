<?php

namespace App\Components\Filters;

use Illuminate\Http\Request;

class SchoolFilters extends QueryFilters
{

    /**
     * Ordering data by name
     */
    public function q($value = 'all') {
        return (!$this->requestAllData($value)) ? $this->builder->where('schools.name', 'like', '%'.$value.'%') : null;
    }

}