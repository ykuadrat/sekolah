<?php

namespace App\Components\Filters;

use Illuminate\Http\Request;

class StudentFilters extends QueryFilters
{

    /**
     * Ordering data by name
     */
    public function q($value = 'all') {
        return (!$this->requestAllData($value)) ? $this->builder->where('students.name', 'like', '%'.$value.'%') : null;
    }

}