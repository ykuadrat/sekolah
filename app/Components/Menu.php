<?php

namespace App\Components;

/**
 * 
 */
class Menu
{
	
	public static function get()
	{
		return [
			[
				'menu' => [
					['label' => 'Dashboard', 'url' => route('dashboard'), 'icon' => 'fa fa-home'],
				]
			],
			[
				'section' => 'Master Data',
				'menu' => [
					['label' => 'Data Siswa', 'url' => 'student/*', 'icon' => 'fa fa-child', 'sub-menu' => [
						['label' => 'Kelas', 'url' => route('class.list')],
						['label' => 'Siswa', 'url' => route('student.list')],
					]],
					['label' => 'Staff', 'url' => 'staff/*', 'icon' => 'fa fa-user-tie', 'sub-menu' => [
						['label' => 'Guru', 'url' => '#'],
					]],
				],
			],
			[
				'section' => 'Absensi',
				'menu' => [
					['label' => 'Siswa', 'url' => '#', 'icon' => 'flaticon-calendar-with-a-clock-time-tools'],
					['label' => 'Guru', 'url' => '#', 'icon' => 'flaticon-calendar-with-a-clock-time-tools'],
				]
			],
			[
				'section' => 'Pembayaran',
				'menu' => [
					['label' => 'SPP', 'url' => '', 'icon' => 'fa fa-dollar-sign'],
					['label' => 'Pembayaran Lainnya', 'url' => '#', 'icon' => 'la la-money'],
				]
			],
		];
	}

}
