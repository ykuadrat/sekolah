<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InitTemplate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:template';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialing Project Template';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $name = $this->secret('What is your name?');

        // $this->info('Your name is '. $name);

        // $this->table(['id', 'name', 'created_at', 'updated_at', 'deleted_at'], \App\Cluster::limit(10)->get()->toArray());
    }
}
