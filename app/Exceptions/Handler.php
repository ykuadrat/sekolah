<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $res['status'] = false;
        $code = 500;

        if ($request->wantsJson()) {
            if ($exception instanceof ValidationException) {
                $res['validation'] = $exception->errors();
                $code = 422;
            }

            if ($exception instanceof TokenExpiredException) {
                $res['msg'] = "Token Expired.";
                $code = $exception->getStatusCode();

            } if ($exception instanceof TokenInvalidException) {
                $res['msg'] = "Token Invalid.";
                $code = $exception->getStatusCode();

            } if ($exception instanceof JWTException    ) {
                $res['msg'] = "Token Absent.";
                $code = $exception->getStatusCode();
            }

            $res['sysMsg'] = $exception->getMessage();
            $res['exClass'] = get_class($exception);
            return response()->json($res, $code);
        }

        return parent::render($request, $exception);
    }
}
