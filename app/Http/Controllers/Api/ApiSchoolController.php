<?php

namespace App\Http\Controllers\Api;

use App\Models\School;
use App\Components\Filters\SchoolFilters;
use App\Components\Traits\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;

class ApiSchoolController extends Controller
{

    use ApiController;

    public function datatable()
    {
        $schools = School::query();

        return \DataTables::of($schools)
                          ->addColumn('action', function($row){
                            return \DatatableBuilderHelper::button([
                                'edit' => [
                                    'url' => route('school.update', ['id' => $row->id]), 
                                    'data' => str_replace('"', "'", $row->toJson()),
                                    'title' => 'Edit School'
                                ],
                                'delete' => route('school.destroy', ['id' => $row->id]),
                            ]);
                          })
                          ->make(true);
    }

    public function index(SchoolFilters $filters)
    {
        $paginate = request()->has('paginate') && request()->paginate > 0 ? request()->paginate : 100; 
        $schools = School::filter($filters)->simplePaginate($paginate);

        return $this->sendRes($schools);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $school = new School;
        $school->fillAndValidate()->save();

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $school]);
        }

        return $school;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $school = School::findOrFail($id);

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $school]);
        }

        return $school;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $school = School::findOrFail($id);
        $school->fillAndValidate()->save();

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $school]);
        }

        return $school;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school = School::findOrFail($id);
        $school->delete();

        if (request()->wantsJson()) {
            return $this->sendRes(['data' => $school]);
        }

        return $school;
    }
}
