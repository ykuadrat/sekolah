<?php

namespace App\Http\Controllers;

use App\Models\ClassModel;
use App\Http\Controllers\Api\ApiClassController;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Class',
            'icon' => 'fa fa-home',
            'breadcrumb' => [
                ['label' => 'Class', 'url' => url()->current(), 'icon' => '']
            ]
        ];

        return view('master_data.class.class_index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Create New Class',
            'icon' => 'fa fa-plus',
            'breadcrumb' => [
                ['label' => 'Class', 'url' => route('class.list')],
                ['label' => 'Create New', 'url' => url()->current()]
            ],
            'class' => new ClassModel
        ];

        return view('master_data.class.class_form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        (new ApiClassController)->store();

        return redirect()->route('class.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClassModel  $class
     * @return \Illuminate\Http\Response
     */
    public function show(ClassModel $class)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClassModel  $class
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class = (new ApiClassController)->show($id);
        $data = [
            'title' => 'Edit Class',
            'icon' => 'fa fa-plus',
            'breadcrumb' => [
                ['label' => 'Class', 'url' => route('class.list')],
                ['label' => $class->name, 'url' => '#'],
                ['label' => 'Edit', 'url' => '#']
            ],
            'class' => $class
        ];

        return view('master_data.class.class_form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClassModel  $class
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        (new ApiClassController)->update($id);

        return redirect()->route('class.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClassModel  $class
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        (new ApiClassController)->destroy($id);

        return redirect()->route('class.list');
    }
}
