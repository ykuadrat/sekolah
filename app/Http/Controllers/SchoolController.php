<?php

namespace App\Http\Controllers;

use App\Models\School;
use App\Http\Controllers\Api\ApiSchoolController;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'School',
            'icon' => 'fa fa-home',
            'breadcrumb' => [
                ['label' => 'School', 'url' => url()->current(), 'icon' => '']
            ]
        ];

        return view('master_data.school.school_index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Create New School',
            'icon' => 'fa fa-plus',
            'breadcrumb' => [
                ['label' => 'School', 'url' => route('school.list')],
                ['label' => 'Create New School', 'url' => url()->current()]
            ],
            'school' => new School
        ];

        return view('master_data.school.school_form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        (new ApiSchoolController)->store($request);

        return redirect()->route('school.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'title' => 'Create New School',
            'icon' => 'fa fa-plus',
            'breadcrumb' => [
                ['label' => 'School', 'url' => route('school.list')],
                ['label' => 'Create New School', 'url' => url()->current()]
            ],
            'school' => (new ApiSchoolController)->show($id)
        ];

        return view('master_data.school.school_form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        (new ApiSchoolController)->update($id);

        return redirect()->route('school.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        (new ApiSchoolController)->destroy($id);

        return redirect()->route('school.list');
    }
}
