<?php

namespace App\Models;

use App\Components\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends BaseModel
{

	use SoftDeletes;

	protected $guarded = [];

	public static function rule($school)
	{
		return [
			'name' => 'required|string|unique:schools,name,' . $school->id
		];
	}

}
