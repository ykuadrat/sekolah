<?php

return [
	'elOptions' => [
		'class' => "table table-striped table-bordered"
	],
	'pluginOptions' => [
		"pagingType" => "numbers",
		"ajax" => [
			"type" => "POST"
		]
	],
	'buttonTemplates' => [
		'edit' => '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details" onclick="showModalForm({
				target: \'#modalEdit\', 
				title: \'<<title>>\', 
				action: \'<<url>>\',
				method: \'PUT\'
			}, <<data>>)"> <i class="la la-edit"></i> </a>',

		'delete' => '<a href="#" onclick="event.preventDefault(); deleteForm(\'<<url>>\', $(this))" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="la la-trash"></i> </a> '
	]
];