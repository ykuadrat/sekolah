<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('grade');
            $table->unsignedInteger('school_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('school_id')->references('id')->on('schools')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address')->nullable();
            $table->unsignedInteger('school_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('school_id')->references('id')->on('schools')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('student_class', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('class_id');
            $table->unsignedInteger('student_id');
            $table->string('generation');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('class_id')->references('id')->on('classes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('student_id')->references('id')->on('students')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DROPING STUDENT CLASS TABLE
        if (Schema::hasColumn('student_class', 'class_id')) {
            Schema::table('student_class', function($table) {
                $table->dropForeign(['class_id']);
            });
        }
        if (Schema::hasColumn('student_class', 'student_id')) {
            Schema::table('student_class', function($table) {
                $table->dropForeign(['student_id']);
            });
        }
        Schema::dropIfExists('student_class');


        // DROPING STUDENT TABLE
        if (Schema::hasColumn('students', 'school_id')) {
            Schema::table('students', function($table) {
                $table->dropForeign(['school_id']);
            });
        }
        Schema::dropIfExists('students');


        // DROPING CLASS TABLE
        if (Schema::hasColumn('classes', 'school_id')) {
            Schema::table('classes', function($table) {
                $table->dropForeign(['school_id']);
            });
        }
        Schema::dropIfExists('classes');


        // DROPING SCHOOL TABLE
        Schema::dropIfExists('schools');
    }
}
