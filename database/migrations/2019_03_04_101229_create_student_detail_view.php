<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentDetailView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS student_detail_view');

        DB::statement("
            CREATE VIEW `student_detail_view` AS
                SELECT 
                s.*,
                #(SELECT MAX(id) FROM student_class sc WHERE sc.student_id = s.id) as class_relation_id,
                (
                    SELECT c.name FROM student_class sc
                    JOIN classes c
                    ON sc.class_id = c.id
                    WHERE sc.id = (SELECT MAX(id) FROM student_class sc WHERE sc.student_id = s.id)
                ) AS class_name,
                sch.name AS school_name

            FROM students s
            JOIN schools sch
            ON s.`school_id` = sch.`id`
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS student_detail_view');
    }
}
