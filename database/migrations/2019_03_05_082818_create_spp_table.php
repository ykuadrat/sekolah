<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spp', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('school_id');
            $table->date('period');
            $table->decimal('amount', 14, 4)->nullable();
            $table->timestamp('paid_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DROPING STUDENT CLASS TABLE
        Schema::table('spp', function($table) {
            if (Schema::hasColumn('spp', 'student_id')) {
                $table->dropForeign(['student_id']);
            }
            if (Schema::hasColumn('spp', 'school_id')) {
                $table->dropForeign(['school_id']);
            }
        });
        Schema::dropIfExists('spp');
    }
}
