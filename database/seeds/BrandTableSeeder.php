<?php

use App\Brand;
use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=0; $i < 1000; $i++) { 
    		Brand::updateOrCreate([
                'name' => 'Brand ' . $i
                'category_id' => $i + 1
            ]);
    	}
    }
}
