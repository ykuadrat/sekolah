<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=0; $i < 1000; $i++) { 
    		Category::updateOrCreate([
                'name' => 'Category ' . $i
                'cluster_id' => $i + 1
            ]);
    	}
    }
}
