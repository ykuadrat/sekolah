<?php

use App\Cluster;
use Illuminate\Database\Seeder;

class ClusterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=0; $i < 1000; $i++) { 
    		Cluster::updateOrCreate(['name' => 'Cluster ' . $i]);
    	}
    }
}
