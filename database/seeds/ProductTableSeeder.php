<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=0; $i < 1000; $i++) { 
    		Product::updateOrCreate([
                'name' => 'Product ' . $i
                'category_id' => $i + 1
            ]);
    	}
    }
}
