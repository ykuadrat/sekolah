@extends('layouts.app')

@section('content')
{{ Form::open(['url' => route('submit-dashboard'), 'class' => 'm-form m-form--state', 'id' => 'formTest'])}}
	{{ 
		Form::textInput('form_validation', null, [
			'elOptions' => [
				{{-- 'required' => 'required', --}}
				'minLength' => '1',
				'id' => 'formV'
			],
			'addons' => ['position' => 'right', 'text' => 'test']
		]) 
	}}

	{{-- {{ 
		Form::dateInput('product_family_name', null, [
			'labelContainerClass' => 'col-md-3',
			'elOptions' => [
				'required' => ''
			]
		])
	}}

	{{
		Form::select2MultipleInput('testtt', [], route('sample-api'), [
			'text' => 'obj.item_name + " (" + obj.weebz_pn + ")"',
			'key' => 'item_name',
			'labelText' => 'Sample', 
			'elOptions' => ['placeholder' => 'Select sample...'],
		])
	}}

	{{
		Form::multipleColumnInput('select2_sample2', null, [
			[
				'type' => 'text',
				'name' => 'test1',
				'elOptions' => [
					'placeholder' => 'Please enter text 1 here'
				]
			], 
			[
				'type' => 'number',
				'name' => 'test2',
				'elOptions' => [
					'placeholder' => 'Please enter text 2 here',
				]
			], 
		])
	}} --}}

	{{
		Form::select2Input('select2_sample', null, route('sample-api'), [
			//'text' => ['field' => ['item_name', 'weebz_pn', 'description'], 'template' => '<<field>> (<<field>>) || <<field>>'],
			{{-- 'text' => 'obj.item_name + " (" + obj.weebz_pn + ")"', --}}
			'labelText' => 'Sample',
			'info' => 'Notes: Need to be check honestly',
			'elOptions' => ['placeholder' => 'Select sample...'],
			'ajaxParams' => [
				'filter1' => '$("#formV").val()'
			]
		])
	}}

	<button type="submit" class="btn btn-success">Submit</button>

{{ Form::close() }}


	<div class="m-portlet__body">

		{!! 
			DatatableBuilderHelper::render(route('student.datatable'), [
				'columns' => [
					[
						'attribute' => 'item_name',
						'label' => 'Name',
					],
					'item_name',
					'description', 
					'keyword', 
					'Action', 
				],
				'elOptions' => [
					'id' => 'tableSample'
				],
				'pluginOptions' => [
					'searching' => false,
				],
				'filters' => [
					'item_name' => '$("#formV").val()',
				]
			]);
		!!}

		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply
		dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
		<div class="m-separator m-separator--space m-separator--dashed"></div>
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply
		dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply
		dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
		<div class="m-separator m-separator--space m-separator--dashed"></div>
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply
		dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
		<div class="m-separator m-separator--space m-separator--dashed"></div>
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply
		dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply
		dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
		<div class="m-separator m-separator--space m-separator--dashed"></div>
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply
		dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
		<div class="m-separator m-separator--space m-separator--dashed"></div>
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply
		dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply
		dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
	</div>
@endsection

@section('additional-js')
	<script type="text/javascript">
		$("#formTest").validate({
			debug: true,
			errorElement: 'span',
			highlight: function (element) { // hightlight error inputs
                // set error class to the control group   
                $(element).closest('.form-group').removeClass("has-success").addClass('has-danger');                   
            },
			errorPlacement: function(error, element){
				var formGroup = element.closest('.form-group')
				$(formGroup).addClass('has-danger')
				$(formGroup).find('.error-container').html(error)
			}, 
			success: function (label, element) {
				var formGroup = element.closest('.form-group')
				$(formGroup).removeClass('has-danger').addClass('has-success')
			},
			submitHandler: function(form){
				
				console.log(dtOptions.ajax.data.filters)
				dataTableFilter_tableSample();
				// form.submit();
			}
		});
	</script>
@endsection