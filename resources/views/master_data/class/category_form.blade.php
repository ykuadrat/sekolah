@extends('layouts.app', [
	'stickyButton' => [
		['label' => 'Submit Form', 'icon' => 'la la-check', 'elOptions' => [
			'onclick' => "$('#fb_category').submit()"
		]]
	]
])

@section('content')
	{{ Form::model($category, ['id' => 'fb_category', 'method' => $category->isNewRecord() ? 'POST' : 'PUT']) }}

		{{ Form::textInput('name', old('name')) }}

		{{ Form::select2Input('cluster_id', null, route('cluster.data') . '?paginate=25') }}

	{{ Form::close() }}
@endsection