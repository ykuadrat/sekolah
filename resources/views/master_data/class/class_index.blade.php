@extends('layouts.app', [
	'toolbarButton' => [
		['label' => 'Create new class', 'icon' => 'la la-plus', 'elOptions' => [
			'onclick' => "showModalForm({
				target: '#modalEdit', 
				title: 'Create New Class', 
				action: '". route('class.store') ."',
			})"
		]],
	]
])

@section('content')

{!! 
	DatatableBuilderHelper::render(route('class.datatable') . '?include=school', [
		'columns' => ['name', 'grade', 'school.name', 'action'],
		'elOptions' => [
			'id' => 'test',
		],
	])
!!}

@include('components.modal_form_ajax', [
	'id' => 'modalEdit',
	'tableTarget' => 'test',
	'fields' => [
		Form::textInput('name', old('name')),
		Form::numberInput('grade', old('grade')),
	]
])

@endsection