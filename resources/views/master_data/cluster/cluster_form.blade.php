@extends('layouts.app', [
	'stickyButton' => [
		['label' => 'Submit Form', 'icon' => 'la la-check', 'elOptions' => [
			'onclick' => "$('#fb_cluster').submit()"
		]]
	]
])

@section('content')
	{{ Form::model($cluster, ['id' => 'fb_cluster', 'method' => $cluster->isNewRecord() ? 'POST' : 'PUT']) }}

		{{ Form::textInput('name', old('name')) }}

	{{ Form::close() }}
@endsection