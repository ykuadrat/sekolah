@extends('layouts.app', [
	'toolbarButton' => [
		['label' => 'Create new cluster', 'icon' => 'la la-plus', 'elOptions' => [
			'onclick' => "showModalForm({
				target: '#modalEdit', 
				title: 'Create New Cluster', 
				action: '". route('cluster.store') ."',
			})"
		]],
	]
])

@section('content')

{!! 
	DatatableBuilderHelper::render(route('cluster.datatable'), [
		'columns' => ['name', 'action'],
		'elOptions' => [
			'id' => 'test',
		]

	])
!!}

@include('components.modal_form_ajax', [
	'id' => 'modalEdit',
	'tableTarget' => 'test',
	'fields' => [
		Form::textInput('name', old('name'))
	]
])

@endsection