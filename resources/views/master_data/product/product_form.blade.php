@extends('layouts.app', [
	'stickyButton' => [
		['label' => 'Submit Form', 'icon' => 'la la-check', 'elOptions' => [
			'onclick' => "$('#fb_product').submit()"
		]]
	]
])

@section('content')
	{{ Form::model($product, ['id' => 'fb_product', 'method' => $product->isNewRecord() ? 'POST' : 'PUT']) }}

		{{ Form::textInput('name', old('name')) }}

		{{
			Form::select2Input('brand_id', $product->isNewRecord() ? null : [$product->brand_id, $product->brand->name], route('brand.data', ['paginate' => 10]))
		}}

	{{ Form::close() }}
@endsection