@extends('layouts.app', [
	'toolbarButton' => [
		['label' => 'Create new product', 'icon' => 'la la-plus', 'elOptions' => [
			'onclick' => "showModalForm({
				target: '#modalEdit', 
				title: 'Create New Product', 
				action: '". route('product.store') ."',
			})"
		]],
	]
])

@section('content')

{!! 
	DatatableBuilderHelper::render(route('product.datatable') . '?include=brand', [
		'columns' => ['name', 'brand.name', 'action'],
		'elOptions' => [
			'id' => 'test',
		],

	])
!!}

@include('components.modal_form_ajax', [
	'id' => 'modalEdit',
	'tableTarget' => 'test',
	'fields' => [
		Form::textInput('name', old('name')),
		[
			'field' => Form::select2Input('brand_id', null, route('brand.data') . '?paginate=25', [
				'pluginOptions' => [
					'dropdownParent' => '#modalEdit'
				]
			]),
			'selector' => '#select2-brand_id',
			'type' => 'select2',

			// FOR SETTING DATA WHEN EDIT MODAL SHOWN
			'dataId' => 'brand.id',
			'dataLabel' => 'brand.name'
		]
	]
])

@endsection