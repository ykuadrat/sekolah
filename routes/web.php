<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/sample-api', 'HomeController@sample')->name('sample-api');
Route::post('/submit', 'HomeController@submit')->name('submit-dashboard');


Route::group(['middleware' => 'auth'], function() {
});